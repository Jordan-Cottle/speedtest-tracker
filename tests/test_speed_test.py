""" Simple test module for testing package __init__

"""

from speed_test import __version__


def test_version():
    """Check that the version number has the expected value."""
    assert __version__ == "0.1.0"
