"""Pydantic models for tracking structured data. """
import logging
from typing import Optional, Union

from pydantic import BaseModel, Field, AnyUrl, HttpUrl, BaseSettings


class Server(BaseModel):
    """Server used for a speed test."""

    url: HttpUrl
    latitude: float = Field(alias="lat")
    longitude: float = Field(alias="lon")
    name: str
    country: str
    country_code: str = Field(alias="cc")
    sponsor: str
    id: int
    host: AnyUrl
    latency: float


class SpeedtestResult(BaseModel):
    """Summary of results from a speedtest."""

    download: float
    upload: float
    ping: float
    bytes_sent: int
    bytes_received: int
    share: Optional[HttpUrl]


class Settings(BaseSettings):
    """Configuration for speedtest routine."""

    interval_seconds: float = 30
    metrics_port: int = 8000
    log_level: Union[int, str] = logging.DEBUG
