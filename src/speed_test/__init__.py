""" Run a speed test periodically and expose metrics for prometheus to collect. """

__version__ = "0.1.0"
