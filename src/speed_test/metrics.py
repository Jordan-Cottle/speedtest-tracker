""" Module for metrics related setup. """

from prometheus_client import Gauge

from .models import SpeedtestResult

DOWNLOAD_SPEED = Gauge("download_speed", "Download speed in bits/s")
UPLOAD_SPEED = Gauge("upload_speed", "Upload speed in bits/s")
PING = Gauge("ping", "Latency in milliseconds")


def report_results(speedtest_result: SpeedtestResult) -> None:
    """Publish speed test results via prometheus metrics."""

    DOWNLOAD_SPEED.set(speedtest_result.download)
    UPLOAD_SPEED.set(speedtest_result.upload)
    PING.set(speedtest_result.ping)
