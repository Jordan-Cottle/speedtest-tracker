"""Module for managing functions related to gather speed test data. """

import logging

from speedtest import Speedtest  # type: ignore

from .models import SpeedtestResult

LOGGER = logging.getLogger(__name__)


def setup() -> Speedtest:
    """Setup the speedtest instance."""

    speedtest: Speedtest = Speedtest()
    LOGGER.info("Locating best server for speed test")
    speedtest.get_best_server()

    return speedtest


def run_test(speedtest: Speedtest) -> SpeedtestResult:
    """Run both the upload and download tests and return their results."""

    LOGGER.debug("Testing download speed..")
    speedtest.download()
    LOGGER.debug("Testing upload speed..")
    speedtest.upload()

    LOGGER.debug("Speedtest complete")
    return SpeedtestResult.parse_obj(speedtest.results.dict())


def share(speedtest: Speedtest) -> str:
    """Publish results and return a sharable link."""

    speedtest.share()

    return str(speedtest.results["share"])
