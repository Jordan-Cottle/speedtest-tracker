"""Main entrypoint module for the speedtest monitor.

Runs a speed test on an interval and publishes metrics that prometheus can consume.

Log level, metrics server port, and test interval can be configured via environment variables.

Set metrics port:
`export METRICS_PORT=5555`

Set speedtest interval, the value is in seconds between tests:
`export INTERVAL_SECONDS=3600`

Set log level:
`export LOG_LEVEL="INFO"`
"""
import logging
import time

from prometheus_client import start_http_server

from .metrics import report_results
from .models import Settings
from .speed_test import run_test, setup

LOGGER = logging.getLogger("speedtest")


def main() -> None:
    """Run a speed test periodically and publish metrics."""
    settings = Settings()
    logging.basicConfig(level=settings.log_level)
    start_http_server(settings.metrics_port)

    speed_test = setup()
    while True:
        results = run_test(speed_test)

        report_results(results)

        LOGGER.info(f"Speedtest results: {results}")
        time.sleep(settings.interval_seconds)


if __name__ == "__main__":
    main()
