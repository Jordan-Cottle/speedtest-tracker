FROM python:3 as base

ENV VIRTUAL_ENV /.venv
ENV PATH="${VIRTUAL_ENV}/bin:${PATH}"
ENV POETRY_VIRTUALENVS_IN_PROJECT true

FROM base as builder

RUN pip install --no-cache-dir poetry

COPY pyproject.toml poetry.lock ./
# Initial install to get all dependencies, won't install project as src isn't copied yet
RUN poetry install --no-dev

FROM builder as ci

COPY scripts.py .
COPY src/ ./src
COPY tests/ ./tests

# Second install to install project and dev dependencies
RUN poetry install

FROM base as speedtest

COPY --from=builder ${VIRTUAL_ENV} ${VIRTUAL_ENV}
COPY src/ ./src

CMD [ "python", "-m", "src.speed_test.main" ]